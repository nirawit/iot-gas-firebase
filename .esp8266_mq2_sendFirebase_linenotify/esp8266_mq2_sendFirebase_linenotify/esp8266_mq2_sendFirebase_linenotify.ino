#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,20,4);
#include <ESP8266WiFi.h>
#include <TridentTD_LineNotify.h>
#include <FirebaseESP8266.h>
//#include <FirebaseArduino.h>
#define WIFI_SSID "WIFI NAME"
#define WIFI_PASSWORD "xxxxxxxx"

String strChipID;
uint32_t chipID;

//************ Firebase Config ***************//
#define FIREBASE_HOST "https://iot-xxxx.firebaseio.com" //Without http:// or https:// schemes
#define FIREBASE_AUTH "xxxxxxxxxxx"
FirebaseData firebaseData;
//void printResult(FirebaseData &data);
//*********************************//

//************ GET INTERNET TIME SERVER +7 UTC ***************//
#include <NTPClient.h>
#include <WiFiUdp.h>
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

String formattedDate;
String dayStamp;
String timeStamp;
//*********************************//
#define test_btn   D7   // ปุ่มกดเพิ่มค่าอ้างอิง reference ต่อกับขา D6
// token line กลุ่มแจ้งเตือนแก๊สรั่ว  ออก tokenใหม่ได้ที่ https://notify-bot.line.me/my/
String LINE_TOKEN="";


String firebase_uid="xxxxxxx";   // uid จาก Firebase Authen
String firebase_gen_id="-xxxxx";        // Fix id จาก Firebase /Device gen id

//int reference=90;     // ค่าอ้างอิงเริ่มต้น ถ้าตรวจจับได้เกินนี้จะส่งข้อความเข้าไลน์ แก้ที่ตัวเลข 90
int gas=0;     // ตัวแปรเก็บค่าเปอร์เซ็นต์แก๊สที่อ่านได้
//int min_sensor = 50;  // ระดับเซ็นเซอร์น้อยที่สุดที่จะแปลงเป็น  0 %
int max_sensor = 100;  // ระดับเซ็นเซอร์มากสุดที่จะแปลงเป็น 100 %
int lawdata;
//************ SET Millis 1 Sec ทำงานโดยไม่ต้องรอ Delay() ***************//
int startMillis;  //some global variables available anywhere in the program
int currentMillis;
const int period = 30000;  //the value is a number of milliseconds (1000 = 1 วินาที)
//*********************************//
//************ SET Millis 1 Sec ทำงานโดยไม่ต้องรอ Delay() ***************//
int startMillis2;  //some global variables available anywhere in the program
int currentMillis2;
const int period2 = 60000*5;  // 5 นาที (60000 = 1 นาที)
//*********************************//

void setup()
{
  Serial.begin(115200);
  lcd.begin();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0,0); lcd.print("**  Pls add wifi **");
  lcd.setCursor(0,1); lcd.print("**  Search Wifi  **");
  lcd.setCursor(0,2); lcd.print("User : ");lcd.print(WIFI_SSID);
  lcd.setCursor(0,3); lcd.print("Pass : ");lcd.print(WIFI_PASSWORD);

  chipID = ESP.getChipId();
  strChipID = String(ESP.getChipId()).c_str();
  Serial.println();
  Serial.print("strChipID");
  Serial.println(strChipID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

// Initialize a NTPClient to get time
  timeClient.begin();
  timeClient.setTimeOffset(25200); //3600*7 = Thailand +7 = 25200

  startMillis = millis();  //initial start time
  Serial.println("Starting up...");
  firebaseConnect();
  Line_Notify_New(false); // ทดสอบส่งเมื่อทำงาน
  lcd.clear();
}

void loop()
{
  currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)
  if (currentMillis - startMillis >= period){  // ทุก 1 นาที ทำงาน
    getNTP();
    startMillis = currentMillis;  //IMPORTANT to save the start time of the current LED state.

    Readsensor();     // เรียกใช้งานฟังก์ชั่นย่อยชื่อ Readsensor  เพื่ออ่านค่าเซ็นเซอร์แก๊ส
    showdisplay();    // เรียกใช้งานฟังก์ชั่นย่อยชื่อ showdisplay เพื่อแสดงค่าออกทางจอ LCD
  }

  currentMillis2 = millis();  //
  if (currentMillis2 - startMillis2 >= period2)  // ทุก 5 นาที ทำงาน
  {
    startMillis2 = currentMillis2;
    getDataConfigFirebase();
  }


//Readsetup();      // เรียกใช้งานฟังก์ชั่นย่อยชื่อ Readsetup เช็คการกดปุ่ม ทดสอบ

}

void getNTP(){
  while(!timeClient.update()) {
  timeClient.forceUpdate();
  }
  // The formattedDate comes with the following format:
  // 2018-05-28T16:00:13Z
  formattedDate = timeClient.getFormattedDate();
  Serial.println(formattedDate);

  // Extract date
  int splitT = formattedDate.indexOf("T");
  dayStamp = formattedDate.substring(0, splitT);
//  Serial.print("DATE: ");
//  Serial.println(dayStamp);
  // Extract time
  timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
//  Serial.print("HOUR: ");
//  Serial.println(timeStamp);
}
void Readsensor()                 //  ฟังก์ชั่นย่อยทำหน้าที่อ่านค่าเซ็นเซอร์
{
getDataConfigFirebase();
// delay(5000);
lawdata=analogRead(A0);
gas=lawdata*0.1; // (MQ-2 #Detect Value = 1000)

  Serial.print("Readsensor() gas: ");
  Serial.println(gas);

  Serial.print("Readsensor() max_sensor: ");
  Serial.println(max_sensor);
//  Line_Notify_New(false); //test
  if(gas>=max_sensor){
    Line_Notify_New(true); //Send Line Notify
    sendDataAlarmSensorFirebase(); //save AlarmSensor Firebase
  }
  sendDataFirebase();    //save logSensor Firebase
}


void showdisplay()               // ฟังก์ชั่นย่อยแสดงค่าต่าง ๆ ออกทางจอ LCD
{
 lcd.setCursor(0,0);
  lcd.print("* GAS LEAK NOTIFY *");

  lcd.setCursor(0,1);
  lcd.print(" Notify at : ");
  lcd.print(max_sensor);
  lcd.print(" ");
  lcd.setCursor(17,1);
  lcd.print("% ");

  lcd.setCursor(0,2);
  lcd.print("Gas : ");
  lcd.print(lawdata);
  lcd.print("PPM =");
  lcd.setCursor(13,2);
  lcd.print(gas);
  lcd.print("    ");
  lcd.setCursor(17,2);
  lcd.print("% ");

  lcd.setCursor(0,3);
  lcd.print("KEY:");
  lcd.print(strChipID);
  lcd.print("  ");
  lcd.print("WiFi:");
  lcd.print(WIFI_SSID);
//  lcd.print(",");
//  lcd.print(WIFI_PASSWORD);
}

void Readsetup()                 // ฟังก์ชั่นย่อยเช็คการกดปุ่ม ทดสอบ ส่งค่า sensor
{
  if(digitalRead(test_btn)==0) {
  Line_Notify_New(false);
  Serial.println("test_btn: TEST SEND Line_Notify_New");
  }
}

void Line_Notify_New(boolean mode_send)    // mode_send(ค่าเริ่มต้น = true ) | ฟังก์ชั่นย่อยทำหน้าที่ส่งข้อความแจ้งเตือนเข้าไลน์
{
  String message;
//  mode_send = โมดการส่ง true = ส่งจริง | false = ทดสอบส่ง
  if(mode_send==true){
    message = "แจ้งเตือนค่า GAS: "+String(lawdata)+" PPM("+String(gas)+"%) \r\nเกินค่าที่ตั้งไว้ "+max_sensor +" %";
    Serial.println("send Line_Notify_New");
  }else{
    message = "[ทดสอบ]ส่งค่าแจ้งเตือนค่า GAS: "+String(lawdata)+" PPM("+String(gas)+"%) \r\nเกินค่าที่ตั้งไว้ "+max_sensor +" %";
    Serial.println("[ทดสอบ] send Line_Notify_New");
  }
  // กำหนด Line Token
  LINE.setToken(LINE_TOKEN);
  LINE.notify(message);

}

void firebaseConnect(){
 //3. Set your Firebase info
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  //4. Enable auto reconnect the WiFi when connection lost
  Firebase.reconnectWiFi(true);
            //Set the size of WiFi rx/tx buffers in the case where we want to work with large data.
    firebaseData.setBSSLBufferSize(1024, 1024);
    //Set the size of HTTP response buffers in the case where we want to work with large data.
    firebaseData.setResponseSize(1024);

    // GET DATA CONFIG
    getDataConfigFirebase();
}

void sendDataFirebase(){
      String path = "/logSensor/"+strChipID;
//    String jsonStr = "";
    FirebaseJson json1;
    FirebaseJsonData jsonObj;
    String datetime = dayStamp+" "+ timeStamp;
    json1.set("dateTime", datetime);
    json1.set("valueDetect", lawdata);
    if (Firebase.push(firebaseData, path, json1))
    {
        Serial.println("logSensor INSERT DATA");
    }
    else
    {
        Serial.println("FAILED");
        Serial.println("REASON: " + firebaseData.errorReason());
        Serial.println("------------------------------------");
        Serial.println();
    }
 }

void sendDataAlarmSensorFirebase(){
      String path = "/alarmSensor/"+strChipID;
//    String jsonStr = "";
    FirebaseJson json1;
    FirebaseJsonData jsonObj;
    String datetime = dayStamp+" "+ timeStamp;
    json1.set("dateTime", datetime);
    json1.set("valueDetect", lawdata);
    json1.set("valueAlarmDefault", max_sensor); // percen(%) MQ-2 Alert

    if (Firebase.push(firebaseData, path, json1))
    {
        Serial.println("alarmSensor INSERT DATA");
    }
    else
    {
        Serial.println("FAILED");
        Serial.println("REASON: " + firebaseData.errorReason());
        Serial.println("------------------------------------");
        Serial.println();
    }
 }

void getDataConfigFirebase(){
  Serial.println("getDataConfigFirebase start ....");
//     String jsonStr = "";
    if (Firebase.get(firebaseData, "/setting/"+firebase_uid+"/Device/"+firebase_gen_id+"/val_alarm"))
    {
        Serial.println("TYPE: " + firebaseData.dataType());
        String rawmax_sensor = firebaseData.stringData();
        max_sensor = rawmax_sensor.toInt();
        Serial.println("get firebase max_sensor: "+ max_sensor);
//        Serial.println("GET PASSED");
//        Serial.println("PATH: " + firebaseData.dataPath());
//        Serial.print("VALUE: ");
//        if (firebaseData.dataType() == "json")
//        {
//            jsonStr = firebaseData.jsonString();
//            Serial.println(jsonStr);
//            printResult(firebaseData);
//        Serial.println("------------------------------------");
//        Serial.println();
//        }
    }
    else
    {
        Serial.println("FAILED");
        Serial.println("REASON: " + firebaseData.errorReason());
        Serial.println("------------------------------------");
        Serial.println();
    }

     // ## GET LineNotifyToken
    if (Firebase.get(firebaseData, "/setting/"+firebase_uid+"/LineNotifyToken/LineNotifyToken"))
    {
        Serial.println("GET PASSED");
        LINE_TOKEN = firebaseData.stringData();
        Serial.println("LineNotifyToken: "+ LINE_TOKEN);
    }
    else
    {
        Serial.println("FAILED");
        Serial.println("REASON: " + firebaseData.errorReason());
        Serial.println("------------------------------------");
        Serial.println();
    }



 }


void printResult(FirebaseData &data)
{
    if (data.dataType() == "int")
        Serial.println(data.intData());
    else if (data.dataType() == "float")
        Serial.println(data.floatData(), 5);
    else if (data.dataType() == "double")
        printf("%.9lf\n", data.doubleData());
    else if (data.dataType() == "boolean")
        Serial.println(data.boolData() == 1 ? "true" : "false");
    else if (data.dataType() == "string")
        Serial.println(data.stringData());
    else if (data.dataType() == "json")
    {
        Serial.println();
        FirebaseJson &json = data.jsonObject();
        //Print all object data
        Serial.println("Pretty printed JSON data:");
        String jsonStr;
        json.toString(jsonStr, true);
        Serial.println(jsonStr);
        Serial.println();
        Serial.println("Iterate JSON data:");
        Serial.println();
        size_t len = json.iteratorBegin();
        String key, value = "";
        int type = 0;
        for (size_t i = 0; i < len; i++)
        {
            json.iteratorGet(i, type, key, value);
            Serial.print(i);
            Serial.print(", ");
            Serial.print("Type: ");
            Serial.print(type == FirebaseJson::JSON_OBJECT ? "object" : "array");
            if (type == FirebaseJson::JSON_OBJECT)
            {
                Serial.print(", Key: ");
                Serial.print(key);
            }
            Serial.print(", Value: ");
            Serial.println(value);
        }
        json.iteratorEnd();
    }
    else if (data.dataType() == "array")
    {
        Serial.println();
        //get array data from FirebaseData using FirebaseJsonArray object
        FirebaseJsonArray &arr = data.jsonArray();
        //Print all array values
        Serial.println("Pretty printed Array:");
        String arrStr;
        arr.toString(arrStr, true);
        Serial.println(arrStr);
        Serial.println();
        Serial.println("Iterate array values:");
        Serial.println();
        for (size_t i = 0; i < arr.size(); i++)
        {
            Serial.print(i);
            Serial.print(", Value: ");

            FirebaseJsonData &jsonData = data.jsonData();
            //Get the result data from FirebaseJsonArray object
            arr.get(jsonData, i);
            if (jsonData.typeNum == FirebaseJson::JSON_BOOL)
                Serial.println(jsonData.boolValue ? "true" : "false");
            else if (jsonData.typeNum == FirebaseJson::JSON_INT)
                Serial.println(jsonData.intValue);
            else if (jsonData.typeNum == FirebaseJson::JSON_DOUBLE)
                printf("%.9lf\n", jsonData.doubleValue);
            else if (jsonData.typeNum == FirebaseJson::JSON_STRING ||
                     jsonData.typeNum == FirebaseJson::JSON_NULL ||
                     jsonData.typeNum == FirebaseJson::JSON_OBJECT ||
                     jsonData.typeNum == FirebaseJson::JSON_ARRAY)
                Serial.println(jsonData.stringValue);
        }
    }
}
