# iot-gas-firebase

> Firebase auth &amp; GAS Alarm System (RealtimeDB) &amp; FCM

# UI Preview

https://drive.google.com/drive/folders/1XETqBS1BLktzUZdchHRPd2W2YYwsl2Ro?usp=sharing

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

<!--Fix Bug(v-bottom-navigation) ON [npm run generate] => style="padding-top: 8px;" -->

# Ref doc esp8266

https://phatiphatt.wordpress.com/esp32-date-and-time-ntp-client/
<br>
https://randomnerdtutorials.com/esp32-ntp-client-date-time-arduino-ide/
<br>
https://akexorcist.dev/firebase-and-esp8266-with-arduino/

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
