module.exports = {
  apps: [
    {
      name: 'iotGasFirebase',
      instances: '1', // Or a number of instances
      script: 'server/index.js',
      args: 'start',
    },
  ],
}
