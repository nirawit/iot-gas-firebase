const colors = require('vuetify/es5/util/colors').default

module.exports = {
  // mode: 'spa', //v2.14.5 #8044 Deprecate mode option
  ssr: false, // for SPA's
  target: 'static', // 'static or 'default is 'server'
  telemetry: true,
  /*
   ** Customize part subforder url [npm run generate]
   */
  // router: {
  //   base: '/dev/iot-gas-firebase/',
  // },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost,
  },
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'th',
    },
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/fire.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#F50057', height: '5px', continuous: true },
  /*
   ** Global CSS
   */
  css: ['@assets/css/main.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // '~/plugins/firebase.js',
    '~/plugins/notifier.js',
    '~/plugins/breathing-colors.js',
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
  ],
  moment: {
    defaultLocale: 'th',
    locales: ['th'],
    defaultTimezone: 'Asia/Bangkok',
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    [
      'nuxt-gmaps',
      {
        key: 'AIzaSyDzlF4MZhYmb4YN7MmJfzp6uxBSAk0dxiw',
        // you can use libraries: ['places']
      },
    ],
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyCOPOnhtfT2w5W9Mhsd_HuJ9_7wl-gCTIA',
          authDomain: 'iot-somdej17.firebaseapp.com',
          databaseURL: 'https://iot-somdej17.firebaseio.com',
          projectId: 'iot-somdej17',
          storageBucket: 'iot-somdej17.appspot.com',
          messagingSenderId: '954614165983',
          appId: '1:954614165983:web:1a44cf839f55604925146a',
          measurementId: 'G-V66S5QLWH7',
        },
        services: {
          // onFirebaseHosting: true,
          auth: true,
          realtimeDb: true,
          messaging: {
            createServiceWorker: true,
            actions: [
              {
                action: 'randomName',
                url: 'randomUrl',
              },
            ],
            fcmPublicVapidKey:
              'BGnhaDjhoNrJIyMwq4HeoiMTmdI8LpNbx_nFlZhT0VdgxbKe0oBexNL7osatg8M5k8nRLpyvAtbZ-cNxHH6B9kk', // OPTIONAL : Sets vapid key for FCM after initialization
          },
        },
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
