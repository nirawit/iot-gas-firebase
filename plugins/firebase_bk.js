import * as firebase from 'firebase/app'
import 'firebase/auth'
// let app = null
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCOPOnhtfT2w5W9Mhsd_HuJ9_7wl-gCTIA',
  authDomain: 'iot-somdej17.firebaseapp.com',
  databaseURL: 'https://iot-somdej17.firebaseio.com',
  projectId: 'iot-somdej17',
  storageBucket: 'iot-somdej17.appspot.com',
  messagingSenderId: '954614165983',
  appId: '1:954614165983:web:1a44cf839f55604925146a',
  measurementId: 'G-V66S5QLWH7',
}
// Initialize Firebase

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}
export const messaging = firebase.messaging()
export const auth = firebase.auth()
// export const DB = firebase.database()
export const userAuthDB = firebase.database().ref('user_auth')
export const userSettingDB = firebase.database().ref('setting')
export const logSensorDB = firebase.database().ref('logSensor')
export const alarmSensorDB = firebase.database().ref('alarmSensor')
// export const StoreDB = firebase.firestore()
export default firebase
