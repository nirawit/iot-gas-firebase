
importScripts(
  'https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js'
)
importScripts(
  'https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js'
)
firebase.initializeApp({"apiKey":"AIzaSyCOPOnhtfT2w5W9Mhsd_HuJ9_7wl-gCTIA","authDomain":"iot-somdej17.firebaseapp.com","databaseURL":"https:\u002F\u002Fiot-somdej17.firebaseio.com","projectId":"iot-somdej17","storageBucket":"iot-somdej17.appspot.com","messagingSenderId":"954614165983","appId":"1:954614165983:web:1a44cf839f55604925146a","measurementId":"G-V66S5QLWH7"})

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging()

// Setup event listeners for actions provided in the config:
self.addEventListener('notificationclick', function(e) {
  const actions = [{"action":"randomName","url":"randomUrl"}]
  const action = actions.find(x => x.id === e.action.action)
  const notification = e.notification

  if (!action) return

  if (action.url) {
    clients.openWindow(action.url)
    notification.close()
  }
})
